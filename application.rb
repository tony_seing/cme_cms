require 'sinatra'
require 'slim'
require 'yaml'
require 'dm-core'
require 'dm-reflection'


get '/' do
  
  slim :index
end

get '/articles' do


  slim :entry
end

post '/articles' do

  slim :about
end

post '/articles/:id' do


  slim :submit
end
